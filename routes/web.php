<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');
Route::get('/admin/dashboard', [App\Http\Controllers\AdminDashboardController::class, 'dashboard'])->name('admin_dashboard');

Route::get('/user', [App\Http\Controllers\UserController::class, 'listUser'])->middleware('auth')->name('user.index');

// amdmin login 
Route::get('/admin', [App\Http\Controllers\Auth\LoginController::class, 'admin_login_view'])->name('admin_login_view');
Route::post('/admin/login', [App\Http\Controllers\Auth\LoginController::class, 'admin_login'])->name('admin_login');
